import Vue from 'vue'
import App from './App'
import router from './router'
import globalProjectProperties from './global/global-properties'

import {StfSelect, StfSelectOption} from 'stf-vue-select'
import './style/_stf-select.scss'

import SmallBasket from './components/small-basket/small-basket.vue'
import Ordering from './components/ordering/ordering.vue'
import collapse from './components/collapse/collapse.vue'

/* eslint-disable no-new */
Vue.config.productionTip = false

Vue.component('stf-select-option', StfSelectOption)
Vue.component('stf-select', StfSelect)

Vue.component('smallBasket', SmallBasket)
Vue.component('orderingComp', Ordering)
Vue.component('collapse', collapse)

Vue.component('Loader', {
  data () {
    return {
      isVisible: false
    }
  },
  created: function () {
    this.globalProjectProperties.loaderComp = this
  },
  template: `
  <transition name="fade">
    <div class="loader" v-if="isVisible"></div>
  </transition>
  `
})

// add global property
globalProjectProperties.install = function (Vue) {
  Object.defineProperty(Vue.prototype, 'globalProjectProperties', {
    value: globalProjectProperties
  })
}
Vue.use(globalProjectProperties)

// Event Bus
Object.defineProperty(Vue.prototype, '$bus', {
  get: function () {
    return this.$root.bus
  }
})

new Vue({
  el: '#app',
  data: {
    bus: new Vue({})
  },
  router,
  render: h => h(App)
})
