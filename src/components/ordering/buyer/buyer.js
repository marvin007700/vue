export default {
  data () {
    return {
      buyer: null,
      errors: []
    }
  },
  mounted: function () {
    var thisObj = this
    this.$bus.$on('getBuyerInfo', (val) => {
      thisObj.buyer = val
    })
  },
  beforeDestroy () {
    this.$bus.$off('getBuyerInfo')
  }
}
