export default {
  data () {
    return {
      basket: null,
      errors: []
    }
  },
  methods: {
    basketRequest: function () {
      this.globalProjectProperties.getRequest(
        this.globalProjectProperties.url(),
        {
          action: 'getBasket'
        },
        response => {
          this.basket = response.data
          this.$bus.$emit('getProductsSumm', response.data.ITEMS_INFO.FINAL_PRICE)
        },
        error => {
          this.errors.push(error)
        }
      )
    }
  },
  mounted: function () {
    this.basketRequest()
  }
}
