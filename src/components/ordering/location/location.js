var timer
export default {
  data () {
    return {
      payers: null,
      selectedPayer: null,
      locations: null,
      selectedLocation: null,
      searchString: '',
      errors: []
    }
  },
  watch: {
    searchString (after, before) {
      clearTimeout(timer)
      timer = setTimeout(() => {
        this.locationsRequest()
      }, 1000)
    }
  },
  methods: {
    locationsRequest: function () {
      this.globalProjectProperties.getRequest(
        this.globalProjectProperties.url(),
        {
          action: 'getLocations',
          searchString: this.searchString
        },
        response => {
          this.locations = response.data
        },
        error => {
          this.errors.push(error)
        }
      )
    },
    payerRequest: function (id) {
      this.globalProjectProperties.getRequest(
        this.globalProjectProperties.url(),
        {
          action: 'getPayerType',
          payerId: id
        },
        response => {
          this.payers = response.data.payers
          this.selectedPayer = response.data.selectedPayer.ID
          this.$bus.$emit('getBuyerInfo', response.data.selectedPayer)
        },
        error => {
          this.errors.push(error)
        }
      )
    },
    changePayerType: function (event) {
      this.payerRequest(event.target.value)
    }
  },
  mounted: function () {
    this.payerRequest(null)
  }
}
