export default {
  data () {
    return {
      productsSumm: null,
      deliveryPrice: null,
      errors: []
    }
  },
  computed: {
    total: function () {
      return Number(this.productsSumm) + Number(this.deliveryPrice)
    }
  },
  mounted: function () {
    var thisObj = this

    this.$bus.$on('getProductsSumm', (val) => {
      thisObj.productsSumm = val
    })
    this.$bus.$on('getDeliveryPrice', (val) => {
      thisObj.deliveryPrice = val
    })
  },
  beforeDestroy () {
    this.$bus.$off('getProductsSumm')
    this.$bus.$off('getDeliveryPrice')
  }
}
