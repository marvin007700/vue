export default {
  data () {
    return {
      payments: null,
      selectedPayment: null,
      selectedPaymentInfo: null,
      errors: []
    }
  },
  methods: {
    paymentRequest: function (id) {
      this.globalProjectProperties.getRequest(
        this.globalProjectProperties.url(),
        {
          action: 'getPaySystems',
          paySystemId: id
        },
        response => {
          this.payments = response.data.paySystems
          this.selectedPayment = response.data.selectedPaySystem.ID
          this.selectedPaymentInfo = response.data.selectedPaySystem
        },
        error => {
          this.errors.push(error)
        }
      )
    },
    changePayment: function (event) {
      this.paymentRequest(event.target.value)
    }
  },
  mounted: function () {
    this.paymentRequest(null)
  }
}
