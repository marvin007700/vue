export default {
  data () {
    return {
      deliverySystems: null,
      selectedDeliverySystem: null,
      selectedDeliverySystemInfo: null,
      errors: []
    }
  },
  methods: {
    deliverySystemsRequest: function (id) {
      this.globalProjectProperties.getRequest(
        this.globalProjectProperties.url(),
        {
          action: 'getDeliveredSystems',
          deliverySystemId: id
        },
        response => {
          this.deliverySystems = response.data.deliverySystems
          this.selectedDeliverySystem = response.data.selectedDeliverySystem.ID
          this.selectedDeliverySystemInfo = response.data.selectedDeliverySystem
          this.$bus.$emit('getDeliveryPrice', response.data.selectedDeliverySystem.CONFIG.MAIN.PRICE)
        },
        error => {
          this.errors.push(error)
        }
      )
    },
    changeDeliverySystem: function (event) {
      this.deliverySystemsRequest(event.target.value)
    }
  },
  mounted: function () {
    this.deliverySystemsRequest(null)
  }
}
