export default {
  data () {
    return {
      basket: null,
      errors: []
    }
  },
  methods: {
    smallBasketRequest: function () {
      this.globalProjectProperties.getRequest(
        this.globalProjectProperties.url(),
        {
          action: 'getBasket'
        },
        response => {
          this.basket = response.data
        },
        error => {
          this.errors.push(error)
        }
      )
    }
  },
  created: function () {
    // setInterval(() => {
    this.smallBasketRequest(this)
    // }, 5000);
  }
}
