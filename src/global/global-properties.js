import axios from 'axios'

const globalProperties = {
  url: () => {
    return 'http://testmag1.wizard-c.ru/test.php'
  },

  nullCut: (value) => {
    let arr
    let num
    if (value.indexOf('.') !== -1) {
      arr = value.split('.')
    }
    if (value.indexOf(',') !== -1) {
      arr = value.split(',')
    }
    if (arr[1].replace(/0/g, '') === '') {
      num = arr[0]
    } else {
      num = value
    }
    return num
  },

  loaderComp: {},

  showLoader: function (value = false) {
    this.loaderComp.isVisible = value
  },

  getRequest: function (path, params, responseCallback, errorsCallback, showLoader = true) {
    if (showLoader) {
      this.showLoader(true)
    }
    return axios({
      method: 'get',
      url: path,
      params: params
    })
      .then(response => {
        responseCallback(response)
        if (showLoader) {
          this.showLoader(false)
        }
      })
      .catch(error => {
        errorsCallback(error)
        if (showLoader) {
          this.showLoader(false)
        }
      })
  },

  postRequest: function (path, params, responseCallback, errorsCallback, showLoader = true) {
    if (showLoader) {
      this.showLoader(true)
    }
    return axios({
      method: 'post',
      url: path,
      data: params
    })
      .then(response => {
        responseCallback(response)
        if (showLoader) {
          this.showLoader(false)
        }
      })
      .catch(error => {
        errorsCallback(error)
        if (showLoader) {
          this.showLoader(false)
        }
      })
  }
}

export default globalProperties
