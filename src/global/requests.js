import axios from 'axios'

class API {
  constructor () {
    const service = axios.create()
    service.interceptors.response.use(
      response => response,
      error => Promise.reject(error)
    )
    this.service = service
  }

  get (path, callback) {
    return this.service.get(path).then(response => callback(response.status, response.data))
  }

  post (path, payload) {
    return this.service
      .request({
        method: 'POST',
        url: path,
        data: payload
      })
  }
}

export default new API()
