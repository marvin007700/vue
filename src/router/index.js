import Vue from 'vue'
import Router from 'vue-router'

const NotFound = { template: '<h2>Page Not Found</h2>' }

const routes = [
	{
		path: '*',
		component: NotFound
	}
]

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: routes
})
